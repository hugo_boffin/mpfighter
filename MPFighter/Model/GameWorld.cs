using System.Collections.Generic;
using Microsoft.Xna.Framework;

namespace MPFighter.Model
{
    class GameWorld
    {
        public delegate void EntityAddedHandler(GameWorld sender,long id, object entity);

        public event EntityAddedHandler EntityAdded;

        public Dictionary<long, SpaceShip> Ships { get; private set; }

        public List<Bullet> Bullets { get; private set; } 

        public GameWorld()
        {
            Ships = new Dictionary<long, SpaceShip>();
            Bullets = new List<Bullet>();
        }

        public void Tick(GameTime gameTime)
        {
            foreach (var bullet in Bullets)
            {
                bullet.Tick(gameTime);
            }

            Bullets.RemoveAll(x => x.LiveTimeIsOver());
        }

        public void Add(long id, SpaceShip spaceShip)
        {
            Ships.Add(id, spaceShip);

            EntityAdded(this, id, spaceShip);
        }

        public void Add(Bullet bullet)
        {
            Bullets.Add(bullet);
        }

        public bool Remove(Bullet bullet)
        {
            return Bullets.Remove(bullet);
        }
    }
}