using System;
using Microsoft.Xna.Framework;

namespace MPFighter.Model
{
    class SpaceShip
    {
        public float FlySpeed = 0.5f;
        public float RotationSpeed = 0.005f;

        private Vector2 _position;
        private GameWorld _gameWorld;

        private IGun gun;

        public Vector2 Position
        {
            get { return _position; }
            set { _position = value; }
        }

        public float Direction { get; set; }

        public SpaceShip( GameWorld gameWorld,Vector2 position, float direction )
        {
            _gameWorld = gameWorld;
            Position = position;
            Direction = direction;
            this.gun = new SimpleGun(this, _gameWorld);
        }

        public void Move(float factor)
        {
            _position.X += (float)(factor * Math.Sin(Direction));
            _position.Y -= (float)(factor * Math.Cos(Direction));
        }

        public void Rotate(float factor)
        {
            Direction = MathHelper.WrapAngle(Direction + factor);
        }

        public void Shoot()
        {
            gun.Shoot();
        }
    }

    internal interface IGun
    {
        void Shoot();
    }

    class RadialGun : IGun
    {
        private SpaceShip ship;
        private GameWorld gameWorld;

        private int guns = 8;

        public RadialGun(SpaceShip ship, GameWorld gameWorld)
        {
            this.ship = ship;
            this.gameWorld = gameWorld;
        }

        public void Shoot()
        {
            for (int i = 0; i < guns; i++ )
            {
                gameWorld.Add(new Bullet(ship.Position, ship.Direction + MathHelper.TwoPi / (float) guns * i, 0.75f, 1000.0f));
            }
        }
    }

    class SimpleGun : IGun
    {
        private SpaceShip ship;
        private GameWorld gameWorld;

        public SimpleGun(SpaceShip ship, GameWorld gameWorld)
        {
            this.ship = ship;
            this.gameWorld = gameWorld;
        }

        public void Shoot()
        {
            gameWorld.Add(new Bullet(ship.Position, ship.Direction, 0.75f, 1000.0f));
        }
    }

    class TripleGun : IGun
    {
        private SpaceShip ship;
        private GameWorld gameWorld;

        public TripleGun(SpaceShip ship, GameWorld gameWorld)
        {
            this.ship = ship;
            this.gameWorld = gameWorld;
        }

        public void Shoot()
        {
            gameWorld.Add(new Bullet(ship.Position, ship.Direction-MathHelper.Pi*0.125f, 0.75f, 1000.0f));
            gameWorld.Add(new Bullet(ship.Position, ship.Direction, 0.75f, 1000.0f));
            gameWorld.Add(new Bullet(ship.Position, ship.Direction + MathHelper.Pi * 0.125f, 0.75f, 1000.0f));
        }
    }
}