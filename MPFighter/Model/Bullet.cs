using System;
using Microsoft.Xna.Framework;

namespace MPFighter.Model
{
    class Bullet
    {
        //public delegate void LifetimeIsOverHandler(Bullet sender);
        //public event LifetimeIsOverHandler LifetimeIsOver;

        public Vector2 Position;
        private float Direction;
        private float Speed;
        public float Lifetime;

        public Bullet(Vector2 position, float direction, float speed, float lifetime)
        {
            Position = position;
            Direction = direction;
            Speed = speed;
            Lifetime = lifetime;
        }

        public void Tick(GameTime gameTime)
        {
            var factor = Speed * (float) gameTime.ElapsedGameTime.TotalMilliseconds;
            var newPosition = new Vector2(
                Position.X + factor * (float)Math.Sin(Direction),
                Position.Y - factor * (float)Math.Cos(Direction)
                );

            Position = newPosition;

            // Linie old-new -> Intersect
            Lifetime -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
        }

        public bool LiveTimeIsOver()
        {
            return (Lifetime <= 0.0f);
        }
    }
}