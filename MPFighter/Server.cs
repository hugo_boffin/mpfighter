using System;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using MPFighter.Model;

namespace MPFighter
{
    class Server
    {
        private GameWorld gameWorld = new GameWorld();

        readonly NetServer server;

        public Server()
        {
            var config = new NetPeerConfiguration(MpFighterGame.UniqueGameIdentifier);
            config.EnableMessageType(NetIncomingMessageType.DiscoveryRequest);
            config.Port = 14242;

            // create and start server
            server = new NetServer(config);
            server.Start();

            Console.WriteLine("Guten Tag.");
        }

        ~Server()
        {
            server.Shutdown("Gute Nacht.");
        }

        private int interval = 0;

        public void Update(GameTime gameTime)
        {
            interval = (interval + 1)%180;

            //TODO: Pakete verarbeiten
            NetIncomingMessage msg;
            while ((msg = server.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DiscoveryRequest:
                        server.SendDiscoveryResponse(null, msg.SenderEndPoint);
                        Console.WriteLine("DiscoveryRequest");
                        break;
                    case NetIncomingMessageType.VerboseDebugMessage:
                    case NetIncomingMessageType.DebugMessage:
                    case NetIncomingMessageType.WarningMessage:
                    case NetIncomingMessageType.ErrorMessage:
                        Console.WriteLine(msg.ReadString());
                        break;
                    case NetIncomingMessageType.StatusChanged:
                        NetConnectionStatus status = (NetConnectionStatus) msg.ReadByte();
                        if (status == NetConnectionStatus.Connected)
                        {
                            Console.WriteLine(NetUtility.ToHexString(msg.SenderConnection.RemoteUniqueIdentifier) + " connected!");
                            //TODO: Alle Stati

                            msg.SenderConnection.Tag = new SpaceShip(gameWorld,new Vector2(
                                NetRandom.Instance.Next(0, 1024),
                                NetRandom.Instance.Next(0, 600)),
                                NetRandom.Instance.NextSingle()*MathHelper.TwoPi);

                            interval = -1; // allen sofort den "neuen" schicken
                        }

                        break;
                    case NetIncomingMessageType.Data:
                        //Console.WriteLine("...Empfange Bewegung");
                        float x = msg.ReadFloat();
                        float y = msg.ReadFloat();
                        float rot = msg.ReadFloat();
                        
                        SpaceShip ship = (SpaceShip)msg.SenderConnection.Tag;

                        //TODO: fancy movement logic goes here; we just append input to position
                        ship.Position = new Vector2(x, y);
                        ship.Direction = rot;
                        break;
                }
            }
            //TODO: XNA Extensions?
            //gameWorld.Tick(G);

            //TODO: ->Direktes Forwarding ( Signallaufzeit gegenrechnen! )

            //TODO: Alle ??? ms absolute Updates ( Signallaufzeit gegenrechnen! )
            foreach (NetConnection player in server.Connections)
            {
                // ... send information about every other player (actually including self)
                foreach (NetConnection otherPlayer in server.Connections)
                {
                    if (player == otherPlayer && interval != 0) // eigene position nicht ganz so oft updaten
                        continue;

                    if (otherPlayer.Tag == null)
                        continue;
                    
                    // send position update about 'otherPlayer' to 'player'
                    NetOutgoingMessage om = server.CreateMessage();

                    // write who this position is for
                    om.Write(otherPlayer.RemoteUniqueIdentifier);//TODO: RemoteUniqueIdentifier evtl gut??

                    //TODO: SCHOEN loesen!!!
                    SpaceShip ship = (SpaceShip)otherPlayer.Tag;
                    om.Write(ship.Position.X);
                    om.Write(ship.Position.Y);
                    om.Write(ship.Direction);
                    // send message
                    server.SendMessage(om, player, NetDeliveryMethod.Unreliable);
                }
            }
        }
    }
}