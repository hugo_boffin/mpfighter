using System;

namespace MPFighter
{
#if WINDOWS || XBOX
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(string[] args)
        {
            using (MpFighterGame fighterGame = new MpFighterGame())
            {
                foreach (string arg in args)
                {
                    if (arg == "server")
                    {
                        fighterGame.IsServer = true;
                        break;
                    }
                }
                fighterGame.Run();
            }
        }
    }
#endif
}

