using Lidgren.Network;
using Microsoft.Xna.Framework.Input;
using MPFighter.Model;

namespace MPFighter
{
    class HumanoidSpaceShipController
    {
        private readonly SpaceShip playerShip;
        private readonly NetClient client;

        public HumanoidSpaceShipController(GameWorld gameWorld, SpaceShip playerShip, NetClient client)
        {
            this.playerShip = playerShip;
            this.client = client;
        }

        public void Update(KeyboardState keyboardState, float totalMilliseconds)
        {
            bool changed = false;
            if (keyboardState.IsKeyDown(Keys.Up))
            {
                playerShip.Move(totalMilliseconds * playerShip.FlySpeed);
                changed = true;
            }
            if (keyboardState.IsKeyDown(Keys.Down))
            {
                playerShip.Move(totalMilliseconds * -playerShip.FlySpeed);
                changed = true;
            }
            if (keyboardState.IsKeyDown(Keys.Left))
            {
                playerShip.Rotate(totalMilliseconds * -playerShip.RotationSpeed);
                changed = true;
            }
            if (keyboardState.IsKeyDown(Keys.Right))
            {
                playerShip.Rotate(totalMilliseconds * +playerShip.RotationSpeed);
                changed = true;
            }

            if (changed)
            {
                NetOutgoingMessage om = client.CreateMessage();
                om.Write(playerShip.Position.X);
                om.Write(playerShip.Position.Y);
                om.Write(playerShip.Direction);
                client.SendMessage(om, NetDeliveryMethod.Unreliable);
                //Console.WriteLine("Sende Bewegung...");
            }

            if (keyboardState.IsKeyDown(Keys.Space))
            {
                playerShip.Shoot();
            }
        }
    }
}