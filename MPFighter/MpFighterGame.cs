using System;
using System.Collections.Generic;
using Lidgren.Network;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MPFighter.Model;
using MPFighter.View;

namespace MPFighter
{
    public class MpFighterGame : Microsoft.Xna.Framework.Game // : MainController?
    {
        public bool IsServer;
        private Server server;

        private NetClient client;

        private readonly GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private SpriteFont _font1;
        private string _frameRate = "";
        private double _elapsed = 0;
        private double _frames = 0;
        private long PlayerID = 0;

        private readonly List<IView> _views = new List<IView>();

        private GameWorld gameWorld;
        // MainController List
        // MainView List

        private HumanoidSpaceShipController humanoidController;

        public MpFighterGame()
        {
            _graphics = new GraphicsDeviceManager(this)
                            {
                                PreferredBackBufferWidth = 1024,
                                PreferredBackBufferHeight = 600,
                                SynchronizeWithVerticalRetrace = true
                            };

            IsMouseVisible = true;
            IsFixedTimeStep = false;

            Window.AllowUserResizing = true;
            Window.Title = "Revenge of the Flying Space Nazis";

            Console.WriteLine(System.IO.Directory.GetCurrentDirectory());
            Content.RootDirectory = @"..\..\Content"; // Workaround for missing Content Pipeline in VS2013

            //Components.Add(new GamerServicesComponent(this));
        }
        
        protected override void Initialize()
        {
            if (IsServer)
                Console.WriteLine("SERVER");
            else
                Console.WriteLine("CLIENT");

            if (IsServer)
            {
                server = new Server();
            }

            NetPeerConfiguration config = new NetPeerConfiguration(UniqueGameIdentifier);
            config.EnableMessageType(NetIncomingMessageType.DiscoveryResponse);

            client = new NetClient(config);
            client.Start();
            PlayerID = client.UniqueIdentifier;

            client.DiscoverLocalPeers(14242);
            Console.WriteLine("Discovery...");

            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);

            _font1 = Content.Load<SpriteFont>("Arial");
            
            gameWorld = new GameWorld();
            gameWorld.EntityAdded += new GameWorld.EntityAddedHandler(GameWorldEntityAdded);

            bulletTexture = Content.Load<Texture2D>("Particle");
        }

        void GameWorldEntityAdded(GameWorld sender, long id, object entity)
        {
            if (entity is SpaceShip)
            {
                _views.Add(new SpaceShipView(_spriteBatch, _graphics, Content, (SpaceShip) entity));
                if (PlayerID == id)
                {
                    humanoidController = new HumanoidSpaceShipController(gameWorld,(SpaceShip)entity,client);
                }
            }
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            // Update Framerate
            _frames += 1.0;
            _elapsed += gameTime.ElapsedGameTime.TotalSeconds;

            if (_elapsed >= 1.0)
            {
                _frameRate = String.Format("{0:0.00}fps", _frames / _elapsed);
                _elapsed = 0;
                _frames = 0;
            }

            if (server != null)
                server.Update(gameTime);

            // read messages
            NetIncomingMessage msg;
            while ((msg = client.ReadMessage()) != null)
            {
                switch (msg.MessageType)
                {
                    case NetIncomingMessageType.DiscoveryResponse:
                        // just connect to first server discovered
                        client.Connect(msg.SenderEndPoint);
                        Console.WriteLine("GEFUNDEN -> VERBINDE");
                        break;
                    case NetIncomingMessageType.Data:
                        // server sent a position update
                        long who = msg.ReadInt64();
                        float x = msg.ReadFloat();
                        float y = msg.ReadFloat();
                        float rot = msg.ReadFloat();

                        SpaceShip ship;
                        if (gameWorld.Ships.ContainsKey(who))
                        {
                            // dumm, lieber ein connect event
                            ship = gameWorld.Ships[who];
                        }
                        else
                        {
                            ship = new SpaceShip(gameWorld,new Vector2(x, y), rot);
                            gameWorld.Add(who, ship);
                        }

                        //TODO: fancy movement logic goes here; we just append input to position
                        ship.Position = new Vector2(x, y);
                        ship.Direction = rot;
                        break;
                }
            }

            // User Input
            KeyboardState newState = Keyboard.GetState();

            if (humanoidController != null)
            {
                humanoidController.Update(newState, (float)gameTime.ElapsedGameTime.TotalMilliseconds);
            }

            // Network Input
            
            // TODO: NetworkController

            gameWorld.Tick(gameTime);

            // Parent
            base.Update(gameTime);
        }

        private Texture2D bulletTexture;
        public static string UniqueGameIdentifier = "MPFighter001";

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            _spriteBatch.Begin();

            foreach (var bullet in gameWorld.Bullets)
            {
                _spriteBatch.Draw(bulletTexture, bullet.Position, bulletTexture.Bounds,
                    Color.BlueViolet, bullet.Lifetime,
                    new Vector2(bulletTexture.Width * 0.5f, bulletTexture.Height * 0.5f),
                    new Vector2(0.25f), SpriteEffects.None, 0.4f);
            }

            foreach (var view in _views)
            {
                view.Draw();
            }
            
            _spriteBatch.DrawString(_font1, _frameRate, Vector2.Zero, Color.White, 0, Vector2.Zero, 1.0f, SpriteEffects.None, 0.5f);

            _spriteBatch.End();
            
            base.Draw(gameTime);
        }

        protected override void OnExiting(object sender, EventArgs args)
        {
            client.Shutdown("bye");

            base.OnExiting(sender,args);
        }
    }
}
