namespace MPFighter.View
{
    internal interface IView
    {
        void Draw();
    }
}