using MPFighter.Model;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

namespace MPFighter.View
{
    class SpaceShipView : IView
    {
        private readonly Texture2D texture;
        private readonly Vector2 textureCenter;
        private readonly SpaceShip spaceShip;
        private Rectangle shipBounds;

        private ContentManager contentManager;
        private GraphicsDeviceManager graphics;
        private readonly SpriteBatch spriteBatch;

        public SpaceShipView(SpriteBatch spriteBatch, GraphicsDeviceManager graphics, ContentManager contentManager, SpaceShip spaceShip)
        {
            this.spriteBatch = spriteBatch;
            this.graphics = graphics;
            this.contentManager = contentManager;
            this.spaceShip = spaceShip;

            texture = contentManager.Load<Texture2D>("ship");
            textureCenter = new Vector2((float)texture.Width / 2.0f, (float)texture.Height / 2.0f);
        }

        public void Draw()
        {
            shipBounds = texture.Bounds;
            shipBounds.Offset((int)spaceShip.Position.X, (int)spaceShip.Position.Y);
            spriteBatch.Draw(texture, shipBounds, null, Color.White, spaceShip.Direction, textureCenter, SpriteEffects.None, 0.5f);
        }
    }
}